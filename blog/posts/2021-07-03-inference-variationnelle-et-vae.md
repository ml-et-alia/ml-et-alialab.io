---
date: 2021-07-03
title: Inference Variationnelle et VAE
lang: fr-FR
tags:
  - machine learning
  - inference variationnelle
  - auto-encodeurs
  - VAE
author: Rémi Emonet
mathjax:
  presets: '\def\argmax{argmax}'
---

Ce post introduit de zéro les auto-encodeurs variationnels et donc les bases de l'inférence variationnelle.
Nous démarrons avec le concept d'estimation de modèles probabilistes et les notions de paramètres et de variables latentes.

<style>
/* not at the beginning of the page as it would be taken as an abstract */
:root {
  --c-latent: #008000ff;
  --c-distrlatent: #0dff0dff;
  --c-param: #3535ccff;
  --c-distrparam: #00c0ffff;
  --c-obs: black;
  --c-prior: #ff1cffff;
}
strong {color: var(--c-latent); font-weight: normal; }
em {color: var(--c-param); }
comm {opacity: 0.55; }
u {text-decoration: underline; }

div>svg { width: 100%; }
svg .node * { stroke-width:3px !important; }
.LATENT, svg .LATENT * {stroke: var(--c-latent) !important; color: var(--c-latent); }
.DISTRLATENT, svg .DISTRLATENT * {stroke: var(--c-distrlatent) !important; color: var(--c-distrlatent); }
.PARAM, svg .PARAM * {stroke: var(--c-param) !important; color: var(--c-param); }
.DISTRPARAM, svg .DISTRPARAM * {stroke: var(--c-distrparam) !important; color: var(--c-distrparam); }
.OBS, svg .OBS * {stroke: var(--c-obs) !important; color: var(--c-obs); }
.PRIOR, svg .PRIOR * {stroke: var(--c-prior) !important; color: var(--c-prior); }
</style>

### Estimation de modèles probabilistes

On cherche les _paramètres_ que l'on appelle $\theta$ (un ensemble de variables) et les **variables latentes** que l'on appelle $Z = \{z_i\}_i$ d'un modèle probabiliste.
En gros, ici on dira que les paramètres sont des choses (inconnues) globales et les variables latentes des choses (inconnues) qui dépendent des points.

> EX: dans le cas très simple où l'on fait la supposition que nous avons des points tirés d'une gaussienne (loi normale), les paramètres $\theta$ sont la moyenne et variance/covariance de la gaussienne et $Z$ est vide.

On représente généralement cela de la façon suivante <comm>(où le rectangle représente la répétition pour chaque point du dataset, et l'absence de flèche représente l'indépendance conditionnelle entre variables)</comm> :

```mermaid
graph TB
    subgraph plate [ ]
      z((z)):::LATENT-->x((x)):::OBS
    end
    θ((θ)):::PARAM-->x
```

> EX: dans un GMM (mélange de gaussiennes) $\theta$ sont les moyennes, variances <comm>(et poids des gaussiennes, mais on va considérer des poids égaux pour simplifier les explications)</comm>, alors que $z_i$ est l'affectation du i-ème point à une gaussienne donnée

Dans certaines situations, ou à certains moments d'un raisonnement, le dataset est figé donc en fait il n'y a souvent aucune différence entre les deux concepts.
On parle alors indifféremment de variable ou paramètres, pour $\{\theta, Z\}$

> NB: Par contre, dans un VAE, on va faire de l'amortissement (cf bien plus bas), donc on traitera les $Z$ différemment et qui plus est, on ignorera souvent les $\theta$ qui seront parfois cachés dans les dérivations.

La tâche, en général, est de calculer ce que nos observations $X$ nous disent sur nos variables inconnues $\theta$ et $Z$, plus précisément nous voulons estimer la probabilité de nos variables sachant <comm>(étant données)</comm> les données $X$ que nous avons observées, c'est à dire $p(\theta, Z | X)$.

Dans pas mal de cas, on s'intéresse vraiment à cette <u>distribution</u> sur les paramètres, mais parfois on s'intéresse à trouver juste <u>une valeur</u> pour ces paramètres, comme pour le MAP (maximum a posteriori) ou le MLE (maximum likelihood estimator).

Si on fait du MAP (maximum a posteriori), on cherche juste le max (de cette probabilité inconnue), c'est à dire $MAP = \argmax_{\theta, z} p(\theta, Z | X)$.
On a aussi le MLE (maximum likelihood estimator) qui, lui, considère la vraisemblance $MLE = \argmax_{\theta, z} p(X | \theta, Z)$ <comm>(ceci est un peu bizarre si on prend le temps d'y réfléchir... conceptuellement il parait étrange de maximiser une probabilité sur sa condition et non sur le domaine de cette distribution)</comm>.
On peut lier les deux par la règle des probabilités conditionnelles (Bayes) : $p(X | \theta, Z) p(\theta, Z) = p(\theta, Z | X)p(X)$.
Comme on optimise sur $\theta$ et $Z$, $p(X)$ est en fait constant et peut être ignoré d'une certaine façon.
Du coup seul <span class="PRIOR">$p(\theta,Z)$</span> (qui est un a priori sur nos variables inconnues) s'ajoute pour le MAP, donc en gros, MAP est comme MLE mais avec un a priori... ou vu autrement MLE est un MAP avec un prior non informatif (uniforme, si cela existe).

On peut représenter le modèle avec son a priori comme ça :

```mermaid
graph TB
    prior((prior))-->z
    subgraph plate [ ]
      z((z)):::LATENT-->x((x)):::OBS
    end
    θ((θ)):::PARAM-->x
    prior((prior)):::PRIOR-->θ
```

### Utilisation d'un modèle probabiliste

Si on a trouvé des valeurs de $\theta$ et $Z$ sur un ensemble de données, on peut très bien prendre un nouvel ensemble de données, garder $\theta$ et trouver les meilleurs $Z$ (qui dépendent des points) qui correspondent à ces nouveaux points, en travaillant sur $p(Z|\theta, X)$.
On peut aussi garder nos anciens points et tout refaire avec l'union des deux ensembles de données.
Ou encore, on peut résumer tous les anciens points dans un nouveau prior sur $\theta$ (voir sur $Z$ selon sa forme, ce que l'on nomme parfois « posterior predictive »).

> EX: pour un GMM, avec un MAP (ou MLE), on a trouvé notre mélange de gaussienne $\theta$ <comm>(on avait aussi $Z$ qui nous disait à quelle gaussiene chaque point appartient)</comm>.
> On peut alors, pour un nouveau point $x_i$, chercher la distribution sur $z_i$ correspondante $p(z_i|\theta, x_i)$ (pas juste la meilleure valeur) qui nous donne à quel point ce nouveau point appartient à chaque gaussienne (on peut quand même prendre le $\argmax$ si on veux l'affecter à une composante/cluster).

### Inférence Variationnelle de base

Dans l'inférence variationnelle, on considère toujours $p(\theta, Z|X)$, mais on ne veut pas juste trouver le MAP, on veut vraiment la distribution, mais le problème est souvent compliqué.
Du coup, on va chercher les paramètres d'une distribution qui visera à approximer $p(\theta, Z|X)$ (inconnu, induit par nos données $X$), et on notera cette distribution approximante $q_\nu(\theta, Z)$ ($\nu$ sont les paramètres qui contrôlent $q$, et sont parfois omis dans la notation).

> NB: on suppose souvent une sorte d'indépendance dans la distribution approximante, et donc la distribution se décompose en produit de deux termes notés abusément $q$ : $q_\nu(\theta, Z) =$ <span class="DISTRPARAM">$q_\nu(\theta)$</span> <span class="DISTRLATENT">$q_\nu(Z)$</span>
>
> On peut informellement représenter cela comme ça :
>
> ```mermaid
> graph TB
>     qz(("q(z)")):::DISTRLATENT-->z
>     prior((prior)):::PRIOR-->θ
>     qθ(("q(θ)")):::DISTRPARAM-->θ
>     prior((prior))-->z
>     subgraph plate [ ]
>       z((z)):::LATENT-->x((x)):::OBS
>     end
>     θ((θ)):::PARAM-->x
> ```

Le <b>but de l'inférence variationnelle</b> est alors de minimiser la « distance » entre la vraie $p(\theta, Z|X)$ (inconnue) et notre meilleure approximation $q_\nu(\theta, Z)$ (et donc de trouver les meilleurs paramètres $\nu$).
La formulation est de dire que l'on va minimiser la KL divergence ([Kullback–Leibler divergence](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence)) :
$KL(q||p)$ ou, sans raccourci de notation, $KL(q(\theta,Z) || p(\theta,Z|X))$

> EX: pour un GMM, on a beaucoup de paramètres $\theta$ (les moyennes et variances de nos composantes) et encore plus de $Z$ (l'affectation des points). $q$ doit être une distribution sur ces paramètres, donc une distribution sur les moyennes et les variances et une distribution sur les affectations. Pour les variables continues (comme la moyenne et variance) on utilise souvent une gaussienne <comm>(donc une moyenne et une variance pour chaque paramètre)</comm> et pour les variables discrètes (affectation $z_i$) on utilise une loi catégorique (pour chaque point, on a donc $K-1$ poids s'il y a $K$ composantes)... ce qui est énorme s'il y a beaucoup de points.

Dans ce mode d'inférence, on a très souvent plus de paramètres $\nu$ que de variables dans $\theta, Z$.
Cela dit, une estimée ponctuelle (un seul jeu de valeur) pour le vecteur $\nu$ correspond à une distribution sur $\theta, Z$... donc « on y gagne », mais ça peut rester un gros problème surtout si $Z$ est grand (grand nombre de points).

On peut faire quelques développements pour ré-écrire la fonction que l'on optimise :

(minimiser) $KL(q||p) = \mathbb{E}_q[\log\frac{q}{p}] = \mathbb{E}_q[\log q] - \mathbb{E}_q[\log p]$

$\mathbb{E}_q[\log q]$ est la « neg-entropie » (opposé de l'entropie) de la distribution $q$.

On peut retourner l'autre terme :

$\log p = \log p(\theta,Z|X) = \log ( p(X|\theta,Z) p(\theta,Z) / p(X) )$

Et développer le log :

$\log p = \log p(X|\theta,Z) + \log p(\theta,Z) - \log p(X)$

Comme le dataset $X$ est fixe, $\log p(X)$ est une constante que l'on pourra ignorer.
Le premier terme est la log-vraisemblance et le second un log-prior sur les paramètres.
En récapitulant, on trouvera que la $KL$ optimisée en inférence variation est faite de ces deux termes (moyennés sur les valeurs de $Z$ venant de $q$) et de l'entropie de $q$ :

$KL(q||p) = \mathbb{E}_q[\log q] - \mathbb{E}_q[\log p(\theta,Z)] - \mathbb{E}_q[\log p(X|\theta,Z)]$

(à la louche : on veut maximiser l'entropie (rendre la distribution $q$ plutôt uniforme), maximiser l'accord entre $q$ et le prior, et maximiser la log-vraisemblance)

> EX: pour une distribution normale simple de moyenne $\mu$ et d'écart type $\sigma$, l'entropie est (cf [wikipedia](https://fr.wikipedia.org/wiki/Loi_normale#Entropie_et_quantit%C3%A9_d'information)) $0.5 \log(2\pi e \sigma^2)$

On peut aussi re-regrouper les premiers termes (on aurait pu le faire dès le départ) :

$KL(q||p) = \mathbb{E}_q[\log \frac{q}{p(\theta,Z)}] - \mathbb{E}_q[\log p(X|\theta,Z)]$

qui donne aussi

$KL(q||p) = KL(q||p(\theta,Z)) - \mathbb{E}_q[\log p(X|\theta,Z)]$

qui s'interprète assez bien en tant que : minimiser la KL entre $q$ et le prior, tout en maximisant la log-vraisemblance (en moyenne sur $q$).

> EX: pour une distribution $q$ qui est $\mathcal{N}(\mu,\sigma^2)$ et un prior $\mathcal{N}(0,1)$, on a une KL qui vaut $0.5 \sigma^2 + \mu^2 -1-\log(\sigma^2)$ (cf [wikipedia](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence#Multivariate_normal_distributions), qui se démontre en développement la formule) ... que l'on retrouve dans les VAE typiques.

#### Quelle liberté avons-nous en restant en mode « Variational Inference » ?

En restant dans le formalisme et sans rien changer, il est possible de choisir la forme de $q$ et aussi le prior $p(\theta, Z)$.

On peut, plus difficilement je dirais, se dire que l'on va optimiser autre chose que $KL(q||p)$ (qui a ses défauts) (par exemple en mode wasserstein ? $\mathcal{W}(q, p)$, donne Wasserstein Variational Inference ?) mais il faut arriver à faire des dérivations similaires (ce qui n'est pas du tout du tout gagné).

### Que sont donc les VAE ?

#### Préliminaires (ou rappels) : Auto-Encodeur non-variationnel

Un auto-encodeur (AE) est un modèle (par ex, un réseau de neurones) _symétrique_, dans le sens où il a autant de neurone sur la couche de sortie que sur la couche d'entrée.
Un AE peut être imaginé comme fait de deux parties : une première moitié qu'on appelle « *encodeur* » et une deuxième moitié qu'on appelle « *décodeur* ».
On utilise généralement les AE en apprentissage non-supervisé : on leur donne le dataset d'apprentissage sans étiquette, en passant la même valeur à la fois en entrée et en tant que sortie attendue.
Lors de l'entraînement, qui vise à apprendre à la fois l'encodeur et le décodeur, on minimise généralement _l'erreur de reconstruction_, c'est à dire le carré de la distance euclidienne entre la sortie de l'auto-encodeur et la sortie attendue (qui est aussi son entrée).

[wikipedia](https://fr.wikipedia.org/wiki/Auto-encodeur)

Quel intérêt d'apprendre à un réseau de neurones à re-générer des données que l'on a déjà ?
Tout dépend de la forme du réseau : si, comme dans la plupart des AE, il a une forme d'entonnoir, avec une couche centrale de taille beaucoup plus petite que celle des entrées (et donc aussi des sorties), alors la première partie du réseau (encoder) va apprendre à projeter les entrées dans un espace latent (la couche du milieu) plus petit, donc à "compresser" les données (i.e. à trouver les éléments importants à conserver et ceux, moins important à jeter) et la deuxième partie (decoder) va apprendre à reconstruire le mieux possible les données originales à partir de leur représentation latente/compressée.
En général, on va choisir le nombre de neurones sur la couche du milieu plus petit que la taille des couches d'entrée/sortie, pour compresser les données. Cependant, on pourrait aussi choisir une taille de la couche latente/du milieu plus grande que les couches d'entrée/sortie, ce qui forcerait l'AE à chercher une projection dans un espace de plus grande dimension. Dans certains cas, cela peut être utile, par exemple pour chercher une séparation linéaire de ces données dans ce plus grand espace (un peu comme le font les SVM). Dans de tels cas, il faut faire attention 1) à ce que l'AE n'apprenne pas la fonction identité (en fixant ses poids à 1 partout = recopier les entrées sur les sorties) et 2) à ce que l'AE n'apprenne pas par cœur les données (puisqu'il a "plus" de place dans son espace latent que dans l'espace s'entrée). Ce dernier cas peut être résolu en ajoutant de la régularisation (drop out ou terme additionnel dans la fonction de loss).

<!-- Un auto-encodeur apprendra les paramètres de 2 réseaux de neurones : encodeur $enc$ et décodeur $dec$.-->

#### Décodeur

Par rapport à l'inférence variationnelle, la partie la plus directe d'un VAE est le décodeur (qui va traiter indépendamment chaque point).
Étant donnée une représentation latente $z_i$ (quelconque) d'un seul point $x_i$, le décodeur (combiné à la loss) est responsable d'évaluer $p(x_i | z_i, \theta)$.
Le fait que le décodeur ne traite qu'un seul point revient à dire que la vraisemblance de $X$ (sachant $\theta$ et $Z$) se décompose (indépendance conditionnelle), autrement dit $p(X|Z, \theta) = \prod_i p(x_i|z_i,\theta)$, ce qui est la supposition de plein de modèles à variables latentes (type GMM).

> EX: dans un VAE, quand on utilise la distance euclidienne au carré entre le point $x_i$ et le point reconstruit $\tilde{x}_i$ ($\left\|x_i - \tilde{x}_i\right\|^2$, aussi appelée erreur de reconstruction), en fait, ce que l'on dit est que le de décodeur prédit la moyenne d'une loi normale, que l'on peut formaliser comme $p(x_i|z_i,\theta) = \mathcal{N}(dec(z_i), 1)(x_i)$ (la loi centrée en $dec(z_i)$ que l'on évalue en $x_i$). Comme on minimise une log proba (cf l'inférence variationnelle), on retombe sur la norme 2 au carré ($\ell_2^2$) avec $\log \mathcal{N}(dec(z_i), 1) \propto \left\|x_i - \tilde{x}_i\right\|^2$ (à une constante additive près).
> On a pris une variance de 1 ci-dessus, mais avec les calculs on retrouve que cette variance contrôle au final le tradeoff entre reconstruction et régularisation.

> NB: si on utilise une sigmoid en fin de décodeur et une « cross entropy loss », en fait, cela revient à supposer que le décodeur produit le/les paramètres d'une loi de bernoulli (et on optimise alors toujours bien la log-proba $\log Bernoulli(dec(z_i))(x_i)$.

> NB: on pourrait ici explicitement avoir d'autres distributions, plus heavy-tailed, comme une laplace (qui correspond à une pénalité en norme $\ell_1$)

En résumé, le décodeur (+ la loss du réseau, que l'on peut choisir) est un modèle de la vraisemblance, paramétré par (l'architecture et) les poids d'un réseau de neurones.
Les poids (paramètres) du réseau cachent donc ce que l'on appelait $\theta$ précédemment (i.e. $loss(dec(z_i), x_i) = \log p(x_i|z_i, \theta)$).

#### Encodeur

Qu'est ce que donc l'encodeur dans tout ça ?
Nous allons voir ce qu'est l'amortissement (<i>amortization</i>) en inférence variationnelle.

En fait, l'encodeur est quelque chose de totalement différent.
Dans l'inférence variationnelle, un problème récurrent est celui du nombre de paramètres $\nu$ qui dépend du nombre de points observés.

> EX: dans un GMM à 10 composantes, si on a 1M de points, alors $Z$ contient 1M de valeurs (l'affectation de chacun des 1M de points à une composante) et si on veut mettre une distribution $q$ sur ces $Z$, alors elle aura par exemple 1M × 9 paramètres (9 paramètres pour définir les proba de tirage d'un « dé » à 10 faces).

L'amortissement en inférence variationnelle vise à réduire ce nombre de paramètres et surtout à faire qu'il ne dépende pas du nombre de points.
Il permet de procurer une régularité et une capacité d'extrapolation en se basant sur l'espace des observations ($X$).
En inférence variationnelle classique, pour chaque point $i$, un jeu de paramètres $\nu_i$ sert à représenter la distribution de $z_i$.
L'amortissement consiste à apprendre une unique fonction qui, à partir de n'importe quel point $x_i$ arrive à prédire une distribution sur $z_i$.
Les paramètres $\nu_i$ (qui dépendaient du nombre de points) sont remplacés par un jeu de paramètres unique qui défini la fonction qui associe une distribution sur l'espace latent pour n'importe quel point d'entrée.

L'encodeur d'un VAE n'est autre que la fonction issue de l'amortissement.
On a plus formellement, pour n'importe quel $x_i$ (ou même n'importe quel point de l'espace d'entrée, même s'il n'est pas dans le dataset) $q(z_i) = Distribution_{enc(x_i)}(z_i)$.

> NB: très souvent dans un VAE, la distribution variationnelle est choisie comme étant une loi normale, nous avons alors un encodeur qui prédit une moyenne et une variance (ou covariance), qui définit la distribution sur l'espace latent $q(z_i) = \mathcal{N}(enc_\mu(x_i), enc_\sigma(x_i))(z_i)$

### Inférence doublement stochastique

Un autre article pourrait être dédié à ces points mais voici une introduction à l'apprentissage des VAE.

L'apprentissage d'un VAE est très souvent doublement stochastique.

Un premier niveau stochastique est le fait de traiter un sous-ensemble (minibatch) des points à chaque fois (généralisation du SGD aux minibatchs).
On peut considérer un minibatch de taille 1 pour le raisonnement.

Un second niveau stochastique est d'approximer l'espérance sur $q(z_i)$ à l'aide d'un unique tirage, on remplace donc $\mathbb{E}_{q(z_i)}[\log p(x_i|z_i)] = \mathbb{E}_{Distribution_{enc(x_i)}(z_i)}[\log p(x_i|z_i)]$ par un approximateur de l'espérance qui utilise un seul tirage et donne donc $\log p(x_i | z_i)$ avec $z_i \sim Distribution_{enc(x_i)}(z_i)$

#### Reparametrization trick

Comme l'étape de sampling est non-différentiable, on utilise le fait que tirer de $\mathcal{N}(\mu,\sigma)$ est équivalent à tirer de $\mathcal{N}(0,1)$ puis à multiplier par $\sigma$ et ajouter $\mu$, qui fait que le sampling est « isolé » et que le gradient peut bien passer. (à détailler au besoin, mais c'est un trick pour l'implémentation, relativement moins important)
