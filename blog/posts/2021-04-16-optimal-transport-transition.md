---
date: 2021-04-16
title: Optimal slide transition
lang: en-US
tags:
  - machine learning
  - optimal transport
  - slide transition
author: Tanguy Kerdoncuff and Rémi Emonet
---

This post presents the optimal slide transition, which is a software that allows you to have transition between your PDF pages.
But, we're talking about a very specific type of transition: it applies Optimal Transport (OT) between the two pages.
You can have a look at an [example online](https://hv0nnus.github.io/TransitionPDF/pres.html) (give it some time to load).

### How to use the generator

You have to clone the github repository: [Hv0nnus/TransitionPDF](https://github.com/Hv0nnus/TransitionPDF). Then you have to replace the pdf file `Presentation_OT.pdf` and run the following command:

    python3 main.py --qualityx 25 --qualityy 25 --qualityx_pres 50 --qualityy_pres 50 --K 100

Wait until it finishes running, then open `pres.html` in your browser and use the keyboard or the buttons to navigate your presentation.
The code automatically changes your PDF to png to apply optimal transport.
The different tunable parameters are:

- Use higher value of qualityx/y (250) to increase the quality of transition between slide.
- Use higher value of qualityx_pres/y (500) to increase the quality of the slide itself (it will be a PNG).
  Both of those values are more or less the number of pixel in x and y axis.
- K is an hyperparameter of the method used for OT on such big images. The bigger it is, the closer it gets to the real transport.

### How it works

To apply the OT between two images, we are obliged to apply a hierarchical Optimal Transport [method](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.705.5462&rep=rep1&type=pdf) as there is too many points in each image.
The number of points is equal to the number of non-white pixel.
The idea is to apply a K-means clustering (with the parameter K above) in the two images separately.
Then we apply the OT between the centroid of each cluster with a mass equal to the number of points.
We now know the match between all the centroid.
Let's take two of them that are matched together, we don't know the detail transport of the two clusters.
So we simply apply again a hierarchical OT between all the points of the two clusters.
Obviously this is done for each match between the centroids.
