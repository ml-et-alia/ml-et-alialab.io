---
date: 2020-05-30
title: Ready. Set. Blog!
lang: en-US
tags:
  - meta
author: Rémi Emonet
---

Welcome to this blog.
I'll explain here what can be expected to find in this blog.
That being said, nothing is written in stone and this blog will evolve as it wants.

This blog will discuss **Machine Learning** (and Data Mining, Artificial Intelligence, etc.).
The visitor can expect content of various level of maturity including:

- introductory **tutorial-like posts** about various machine learning subjects,
- **explanation**, presentation or popularization of the **scientific articles** of the blog's authors,
- some list of **suggested papers** and links to start on a subject,
- more **spontaneous writings** including discussions, paper reviews, random thoughts, opinions, etc.

This blog is expected to be a multi-author blog but also a **collective blog**: posts will have different authors but also some posts will be written collectively.
This blog is authored by a group of voluntary people, all working in the domains of machine learning research.
The set of authors will evolve over time but, originally, this blog stems from the **Data Intelligence team of the Hubert Curien Laboratory** in Saint-Étienne, France.

Thanks for being here, as gift, we offer you a machine-generated cat picture.
![fake cat picture](https://thiscatdoesnotexist.com)

See you soon!
