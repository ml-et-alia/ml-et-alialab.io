module.exports = {
  //base: '/ml-et-alia/',
  dest: 'public',
  theme: '@vuepress/blog',
  themeConfig: {
    dateFormat: 'YYYY-MM-DD',
    footer: {
      contact: [
        {
          type: 'twitter',
          link: 'https://twitter.com/remiemonet',
        },
        {
          type: 'twitter',
          link: 'https://twitter.com/DataIntelGroup',
        },
      ],
    },
  },
  markdown: { // https://vuepress.vuejs.org/config/#markdown
    plugins: [
    ],
  },
  locales: {
    '/': {
      lang: 'en-US',
    },
    '/fr/': {
      lang: 'fr-FR',
    },
  },
  plugins: [
    [
      'mermaidjs',
      {},
    ],
    [
      'vuepress-plugin-mathjax',
      {
        target: 'svg',
        macros: {'\BLOG': 'Ml-Et-Alia'},
      },
    ],
    [
      '@vuepress/blog',
      {
        directories: [
          {
            id: 'post',
            dirname: 'posts',
            path: '/',
            /*
            pagination: {
              lengthPerPage: 5,
            },
            */
          },
          {
            id: 'draft',
            dirname: 'drafts',
            path: '/drafts/',
          },
        ],
        frontmatters: [
          {
            id: 'tag',
            keys: ['tags'],
            path: '/tag/',
            /*
            layout: 'Tags',
            scopeLayout: 'Tag',
            */
          },
        ],
        sitemap: {
          hostname: 'https://ml-et-alia.gitlab.io',
        },
        feed: {
          canonical_base: 'https://ml-et-alia.gitlab.io',
        },
      },
    ],
  ],
}
