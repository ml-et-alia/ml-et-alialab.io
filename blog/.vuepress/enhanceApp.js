export default ({
  Vue, // the version of Vue being used in the VuePress app
  options, // the options for the root Vue instance
  router, // the router instance for the app
  siteData // site metadata
}) => {

  let clicky = () => {
    let isEnabled = process.env.NODE_ENV === 'production'
    if (isEnabled && typeof window !== 'undefined') {
      let doc = window.document
      let cl = '___vpress_clicky___'
      let d = doc.querySelector('.'+cl)
      if (d == null) {
        d = doc.createElement('div')
        d.classList.add(cl)
        doc.body.appendChild(d)
      }
      let s = (txt, attr={}, tagname='script') => {
        let r = doc.createElement(tagname)
        if (txt == null) {
        } else if (typeof txt == 'string') {
          r.textContent = txt
        } else {
          r.innerHTML = txt[0]
        }
        for (let [a, v] of Object.entries(attr)) {
          r.setAttribute(a, v)
        }
        return r
      }
      d.innerHTML = ''
      d.append(s('var clicky_site_ids = [101258736];'))
      d.append(s(null, {async:'true', src:'//static.getclicky.com/js'}))
      d.append(s(['<p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/101258736ns.gif" /></p>'], {}, 'noscript'))
    }
  }

  clicky()
  router.afterEach(clicky)
}
