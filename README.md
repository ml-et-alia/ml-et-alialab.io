


# Contributing

## How to write stuff

### Getting the local preview (recommended)

First install node.

Then install the necessary dependencies:

~~~
npm install
~~~

Then start the live developpement server:

~~~
npm run dev
~~~

Then you can visit the link provided in the console http://localhost:8080/.


### Creating a new post

To create a new post, you'll need to create a new file, following the convention that you will find in `blog/posts/`.
Drafts, to be shared for review, can be put it `blog/drafts/`.

If this helps, here are some template commands that ensure that the file date, matches the blog date (and thus it's URL).

~~~
cd blog/
d=$(date --iso-8601)  # get today's date like 2020-05-30
cat <<EOF > posts/$d-todo-todo-todo.md
---
date: $(date --iso-8601)
title: Title to be filled later
tags: 
  - machine learning
author: Bob Todo
---

Content to be written later.
EOF
~~~

### Editing a post

To edit a post, just find and modify the corresponding `.md` file in `blog/posts/`.

#### Equations?

If you want to use **latex-like** math equations, you can wrap it in `$` for inline maths, or `$$` for separate equations.
Beware, there are a few **constraints**: other delitimeters (like `\[`) cannot be used, and no space or new line should directly follow the opening `$` or `$$` (`$a+b$` is ok ).

An alternative is to use the custom “latex” container (with a div to avoid any need to escape), as follows:


If you want to define latex commands, you can do it in the post header, as follows:

~~~yaml
---
...
author: ...
mathjax:
  presets: '\def\R{\mathbb{R}}'
---
~~~





## Other stuff

### RULES, GUIDELINES

- must: proper content, image etc attribution, no use without proper right or aggrement to do so (for things that are not our own)
- must: have any draft reviewed by at least one person before publishing it
- should: have any draft reviewed by at least two persons before publishing it
- should: have the code to reproduce illustrations and data
- should: clearly mention the licence of the illustrations and code
- should: follow good writting practice, such as https://www.fresheyesediting.com/2018/03/19/avoid-these-common-scientific-writing-mistakes/ 


- https://vuepress.vuejs.org/guide/markdown.html
- https://github.com/markdown-it/markdown-it

### TODO

TODO comment system (issue-based, see vuepress plugins)

TODO logo (maybe not)? other name?

TODO: decide on hosting, and use the issue-based vssue (see https://vuepress-plugin-blog.ulivz.com/guide/getting-started.html#comment)


TODO: add some useful plugins

- the glossary one


### Post Idea

- on the different kind of bounds
- principle of bayesian approaches (and link with regression)
- OT
- fairness

### Random links

- https://sciencecommunication.blog/2019/08/06/science-popularization-vs-science-communication/
- https://blog.typeset.io/4-common-research-writing-mistakes-and-how-to-fix-them-87fa1ca2b513







---------------
---------------
---------------
---------------
---------------

## How the blog has been created

First install node.

(can use yarn or not)

~~~
npm install -g vuepress
~~~

We create a folder for the blog itself, which allows to have other things beside it.
This also allows us to follow the vuepress guide exactly.

# "resolutions" to fix live-reload at the time of the blog creation https://github.com/vuejs/vuepress/issues/2392#issuecomment-633654899

~~~
# to be able to run, later, `npm run dev`
cat <<'EOF' > package.json
{
  "scripts": {
    "dev": "vuepress dev blog",
    "build": "vuepress build blog",
    "preinstall": "npx npm-force-resolutions"
  },
  "resolutions": {
    "watchpack": "1.6.1"
  }
}
EOF


npm install -D vuepress
npm install -D @vuepress/plugin-blog
npm install -D @vuepress/theme-blog

mkdir blog
cd blog
mkdir .vuepress

cat <<'EOF' > .vuepress/config.js
module.exports = {
  theme: '@vuepress/blog',
  themeConfig: {
    dateFormat: 'YYYY-MM-DD',
    footer: {
      contact: [
        {
          type: 'twitter',
          link: 'https://twitter.com/remiemonet',
        },
        {
          type: 'twitter',
          link: 'https://twitter.com/DataIntelGroup',
        },
      ],
    },
  },
  locales: {
    '/': {
      lang: 'en-US',
    },
    '/fr/': {
      lang: 'fr-FR',
    },
  },
  plugins: [
    [
      '@vuepress/blog',
      {
        directories: [
          {
            id: 'post',
            dirname: 'posts',
            path: '/',
            /*
            pagination: {
              lengthPerPage: 5,
            },
            */
          },
        ],
        frontmatters: [
          {
            id: 'tag',
            keys: ['tags'],
            path: '/tag/',
            /*
            layout: 'Tags',
            scopeLayout: 'Tag',
            */
          },
        ],
        sitemap: {
          hostname: 'https://yourdomain' // TODO
        },
        feed: {
          canonical_base: 'http://yoursite', // TODO
        },
      },
    ],
  ],
}
EOF

# later added (+ content in the config.js)
npm install -D vuepress-plugin-mathjax
npm install -D vuepress-plugin-container

#mkdir .vuepress/layouts

mkdir posts

cat <<EOF > posts/$(date --iso-8601)-starting-this-blog.md
---
date: $(date --iso-8601)
title: Starting This Blog!
tags: 
  - info
  - meta
author: Rémi Emonet
---

bla bla1

bla bla2

EOF


~~~

